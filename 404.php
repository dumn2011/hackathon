<?php
header("HTTP/1.1 404 Not Found");
?>
<!doctype html>
<html lang="en">
<head>

    <title>Oh. It's happened. :C</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="error no-margin clearfix">
<div class="error-main">
    <div class="error-main__content">
        <div class="clearfix" style="float: left; width: 370px;">
            <a href="index.php"> <h1 class="error-code no-margin">404</h1> </a>
            <h2 class="error-message no-margin">Page not found</h2>
        </div>
    </div>
</div>
</body>
</html>
