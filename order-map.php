<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>2gis</title>
    <link rel="stylesheet" href="mobile.css" media="(max-width:767px)">
    <link rel="stylesheet" type="text/css" href="style.css"/>

    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
    <script type="text/javascript">
        var map;

        DG.then(function () {
            map = DG.map('map', {
                center: [50.28, 127.52],
                zoom: 13
            });
            map.geoclicker.enable();

        });
    </script>
</head>
<body>
<header class="header ">
    <div class="header-container">
        <div class="header__icon clearfix">
            <span class="icon icon-bus header__icon"></span>
        </div>
        <div class="header-title__wrap">
            <h1 class="header-title no-margin">
                FreeBus
            </h1>
        </div>
    </div>
</header>
<div class="clearfix map-content">
    <div id="map" class="map-block__settings"></div>
</div>

</body>
</html>