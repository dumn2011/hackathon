<?php
ini_set('display_errors', 'On');

try {
    include ("db.php");
    include ("connection.php");
    if (!empty($_POST['start']) &&  !empty($_POST['end']) ):
        $pStart = $_POST['start'];
        $pEnd = $_POST['end'];

        $DB->query("INSERT INTO `Travel` (`trace_ID`, `start`, `end`, `dirver_ID`) VALUES(NULL, PointFromText('POINT($pStart)'), PointFromText('POINT($pEnd)'), '1')");
        header('Location: index.php');
    endif;
    include "close.php";

} catch (PDOException $e) {
    print "Error: " . $e->getMessage() . "<br/>";
    die();
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>2gis</title>
    <link rel="stylesheet" href="mobile.css" media="(max-width:767px)">
    <link rel="stylesheet" href="laptop_desktop.css" media="(min-width:768px)">

    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" href="fonts.css" type="text/css">

    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
 </head>
<body class="body">
<header class="header ">
    <div class="header-container">
        <div class="header__icon clearfix">
            <span class="icon icon-bus header__icon"></span>
        </div>
        <div class="header-title__wrap clearfix">
            <h1 class="header-title no-margin">
                FreeBus
            </h1>
        </div>
    </div>
</header>

<div class="map-container clearfix">
    <form action="index.php" method="post" class="map-form">

        <div class="map-content">
            <div id="map" class="map-block__settings"></div>
            <input type="submit" class="map__button" value="OK">
        </div>
        <div class="bus__input-cover clearfix">
            <input id="i1" type="text" placeholder="Откуда" name="start" class="bus__input bus__input_left">
            <div id="from_01">
            </div>
        </div>
        <div class="bus__input-cover clearfix">
            <input id="i2" type="text" placeholder="Куда" name="end" class="bus__input bus__input_right">
            <div id="to_01">
            </div>
        </div>
    </form>
</div>

<div class="about-container">
    <div class="about-content">
        <div class="about-top">
            <h3 class="about-title no-margin">About</h3>
        </div>
        <div class="about-bot">
            <p class="about-text no-margin">
                FreeBus - сервис для вызова без маршрутного общественного транспорта в вечернее время суток.
                Он позволяет вам добраться от начального пункта назначения до конечного без пересадок,
                заранее указав ваш маршрут. Общий маршрут строится автоматически в соответствии с
                количеством пассажиров, их пунктами назначения и новыми запросами.
                Динамически изменяясь согласно времени ожидания каждого пассажира, он обеспечивает
                комфортный и безопасный проезд граждан, являясь более дешевой альтернативой единственному
                транспорту в это время - такси. Решение предназначено для небольших городов или городов,
                где отсутствует метрополитен.

            </p>
        </div>
    </div>
</div>


<footer class="footer-container">
    <div class="footer-content">
        <span>Команда разработчиков: Guys With Rice</span>
    </div>
</footer>


<script type="text/javascript">
    var map,
        marker_start,
        marker_end;
    var locationInfo1 = document.getElementById('from_01').value,
        locationInfo2 = document.getElementById('to_01');

    DG.then(function () {
        map = DG.map('map', {
            center: [50.28, 127.52],
            zoom: 13
        });
        map.geoclicker.enable();
        marker_start = DG.marker([50.29, 127.53],{draggable:true}).addTo(map).bindLabel('Старт!', {
            static: true
        });
        marker_start.on('drag', function(e) {
            var lat = e.target._latlng.lat.toFixed(3),
                lng = e.target._latlng.lng.toFixed(3);

            //locationInfo1.innerHTML = lat + ', ' + lng;
            document.getElementById('i1').value = lat + ' ' + lng;
        });
        marker_end = DG.marker([50.28, 127.52],{draggable:true}).addTo(map).bindLabel('Финиш!', {
            static: true
        });
        marker_end.on('drag', function(e) {
            var lat = e.target._latlng.lat.toFixed(3),
                lng = e.target._latlng.lng.toFixed(3);

            //locationInfo2.innerHTML = lat + ', ' + lng;
            document.getElementById('i2').value = lat + ' ' + lng;
        });

    });

</script>
</body>
</html>